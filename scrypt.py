
#sudo pigpiod

# -*- coding: utf-8 -*-

import csv
import time
import datetime
import os

cmd = "sudo pigpiod"

returned_value = os.system(cmd)

import pigpio

pi = pigpio.pi()

import DHT22

s = DHT22.sensor(pi, 4)

s.trigger()

with open('test06.csv', 'a') as f:
	 writer = csv.writer(f, delimiter=';')
	 writer.writerow(['drct','temperature','humidity'])


    while True:

        now = datetime.datetime.now()

        drct = now.strftime("%H:%M:%S")
        print drct

        s.trigger()

        temperature = ('{:3.2f}'.format(s.temperature() / 1.))
        print temperature +u' \u2103'

        humidity = ('{:3.2f}'.format(s.humidity() / 1.))
        print humidity +' %'

    	 for i in range(1):
    		writer.writerow([drct, temperature, humidity])

        time.sleep(10)
